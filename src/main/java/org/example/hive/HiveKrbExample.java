package org.example.hive;

import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import org.example.table.HiveCurveData;
import org.example.table.HiveCurveDwsData;
import org.example.table.JdbcHiveCurveDwsData;

import java.io.InputStream;

/**
 * @author lwc
 * @description: TODO
 * @date 2023/12/19 20:29
 */
@Slf4j
public class HiveKrbExample {

    public static void main(String[] args) {

        SparkConf sparkConf = new SparkConf().setAppName("Spark HBase Example");
        SparkSession spark = null;
        if(args.length>=1){
            spark = SparkSession.builder()
                    .appName("Spark Hive Example")
//                    .config("spark.master", "local[*]")  // 使用本地模式，[*]表示使用所有可用的核心
                    .config(sparkConf)
                    .enableHiveSupport()
                    .getOrCreate();
            log.info("加上了local[*]");
        }else {
            spark = SparkSession.builder()
                    .appName("Spark Hive Example")
                    .config("spark.master", "local[*]")  // 使用本地模式，[*]表示使用所有可用的核心
                    .config(sparkConf)
                    .enableHiveSupport()
                    .getOrCreate();
        }
        sparkConf.set("spark.shuffle.memoryFraction", "0.6");
        sparkConf.set("spark.shuffle.sort.io.memoryFraction", "0.4");
        // 设置 Kerberos 相关配置
        spark.conf().set("spark.authenticate", "true");
        spark.conf().set("spark.yarn.principal", "your_principal@REALM");
        InputStream keytabInputStream = HiveKrbExample.class.getClassLoader().getResourceAsStream("user.keytab");
        InputStream krb5ConfInputStream = HiveKrbExample.class.getClassLoader().getResourceAsStream("krb5.conf");
        spark.conf().set("spark.yarn.keytab", keytabInputStream != null ? "user.keytab" : "");
        spark.conf().set("java.security.krb5.conf", krb5ConfInputStream != null ? "krb5.conf" : "");

//        HiveSyncDws(spark,sparkConf);
        JdbcHiveCurveDwsData.Data(spark,sparkConf);
    }
}
