package org.example.domin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataDay implements Serializable {

    private String f_data_collection_time;

    private String f_data_input_time;

    private String f_key_name;

    private Long f_measurement_points;

    private Double data_e0;

    private Double data_e1;

    private Double data_e2;

    private Double data_e3;

    private Double data_e4;

    private Long f_delete;

    private static final long serialVersionUID = 1L;

}
