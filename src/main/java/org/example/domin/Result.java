package org.example.domin;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.spark.sql.Row;

import java.util.Date;


/**
 * @author lwc
 * @description: TODO
 * @date 2023/12/22 15:19
 */
@Data
@AllArgsConstructor
public class Result {

    private String phaseFlag;

    private String keyName;

    private String type;

    private String sql;

}
