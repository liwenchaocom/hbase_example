package org.example.domin;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class TobjLdHourVa implements Serializable {

    private String f_data_collection_time;

    private String f_data_input_time;

    private String f_key_name;

    private Long f_measurement_points;

    private Double data_va;

    private Double data_vb;

    private Double data_vc;

    private Long f_delete;

    private static final long serialVersionUID = 1L;

}
