package org.example.utils;

/**
 * @author lwc
 * @description: TODO
 * @date 2023/12/22 23:38
 */
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeConversion {

    public static String convertUToTime(String uValue) {
        // 将 "u" 后面的数字部分解析为小时和分钟
        int hours = Integer.parseInt(uValue.substring(1, 3));
        int minutes = Integer.parseInt(uValue.substring(3, 5));

        // 转换为 LocalTime 对象（Java 8+）
        LocalTime time = LocalTime.of(hours, minutes);
        return time.toString();
    }

    public static void main(String[] args) {
        // 示例用法
        String u0001 = "u0061";
        String u0001Time = convertUToTime(u0001);
        System.out.println(u0001 + ": " + u0001Time);

        String u1234 = "u1234";
        String u1234Time = convertUToTime(u1234);
        System.out.println(u1234 + ": " + u1234Time);

        // 继续处理其他 uxxxx...
    }
}

