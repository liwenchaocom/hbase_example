package org.example.test;

/**
 * @author lwc
 * @description: TODO
 * @date 2023/11/4 18:40
 */
import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.digest.DigestUtil;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.SparkConf;
import scala.Tuple2;

import java.util.Arrays;

public class WordCountApp {
    public static void main(String[] args) {
//        String startDate = DateUtil.beginOfMonth(DateUtil.date()).toString("yyyyMMdd");
//        // 获取三个月后的日期
//        String endDate = DateUtil.offsetMonth(DateUtil.parse(DateUtil.beginOfMonth(DateUtil.date()).toDateStr()), -3).toString("yyyyMMdd");
//
//        System.out.println("当前月份的开始日期: " + startDate);
//        System.out.println("三个月后的日期: " + endDate);
//
//        String encrypted = DigestUtil.md5Hex("YyPdtqznjk_2023#$");
//        //835e9322fdace89655f6868f2d82f5a7
//        System.out.println("Hutool MD5 Encrypted: " + encrypted);
        String startDate = DateUtil.endOfMonth(DateUtil.date()).toString("yyyyMMdd");
        // 获取三个月后的日期
        String endDate = DateUtil.offsetMonth(DateUtil.parse(DateUtil.beginOfMonth(DateUtil.date()).toDateStr()), -3).toString("yyyyMMdd");
        System.out.println("原来"+startDate+"----"+endDate);

        String string = DateUtil.beginOfDay(DateUtil.date()).toString("yyyyMMdd");
        String startDate1 = DateUtil.endOfMonth(DateUtil.date()).toString("yyyyMMdd");
        // 获取三个月后的日期
        String endDate1 = DateUtil.offsetDay(DateUtil.parse(DateUtil.beginOfDay(DateUtil.date()).toDateStr()), -1).toString("yyyyMMdd");
        System.out.println("开始日期："+string + "结束日期"+endDate1);




    }
}
