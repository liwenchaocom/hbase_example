package org.example.test;

import org.example.utils.JDBCUtils;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

/**
 * @author lwc
 * @description: TODO
 * @date 2024/1/2 14:46
 */
public class T {


    public static void main(String[] args) {
        LocalDateTime dateTime = LocalDateTime.now();

        // 生成当前日期的 00:00 开始的时间
        LocalDateTime startDateTime = LocalDateTime.of(dateTime.toLocalDate(), LocalTime.MIN);

        // 定义时间格式化器，用于格式化输出时间
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Connection connection = JDBCUtils.getConnection("jdbc:mysql://123.60.32.104:23310/gf_b?allowMultiQueries=true&useSSL=true&serverTimezone=GMT%2b8",
                "gf001", "VE*BG#2u*Ztw#3M$");
        // 从 00:00 开始，每次增加 1 分钟，直到 23:59
        Random random = new Random();
        while (startDateTime.toLocalTime().isBefore(LocalTime.of(23, 59))) {
            // 将日期时间格式化为字符串输出
            String format = startDateTime.format(formatter);
            // 每次增加 1 分钟
            startDateTime = startDateTime.plusMinutes(1);
            String insert = "insert into t_load_202401charing_data_dc (f_data_collection_time" +
                    ",f_data_input_time,f_key_name,f_measurement_points,data,f_delete) values('" + format + "'" +
                    ",'" + format + "','10128F5001',34573," + random.nextDouble() * 99 + 1 + ",0)";
            JDBCUtils.executeUpdate(connection,insert);

        }
    }

}
