package org.example.test;

import cn.hutool.core.util.NumberUtil;
import lombok.Data;
import org.apache.hadoop.yarn.webapp.hamlet2.Hamlet;

import java.sql.Date;
import java.time.LocalTime;

/**
 * @author lwc
 * @description: TODO
 * @date 2023/12/22 15:17
 */
@Data
public class Test {

    private Long point;
    private Date dataDate;
    private String phaseFlag;
    private String fKeyName = "10128E25";
    private Double dataVa = 0.0;
    private Double dataVb = 0.0;
    private Double dataVc = 0.0;

    public static void main(String[] args) {
        String input = "u2100"; // 假设输入的字符串为 "u2359"

        // 将 "u" 后面的数字部分解析为小时和分钟
        int hours = Integer.parseInt(input.substring(1, 3));
        int minutes = Integer.parseInt(input.substring(3, 5));

        // 转换为 LocalTime 对象（Java 8+）
        LocalTime time = LocalTime.of(hours, minutes);

        // 打印时间
        System.out.println("转换后的时间为：" + time.toString());

        Object dataDouble = "0.1";
        if(NumberUtil.isNumber(dataDouble.toString())){
            System.out.println("1");
        }else {
            System.out.println(2);
        }
    }
}
