package org.example.concost;

/**
 * @author lwc
 * @description: TODO
 * @date 2023/12/22 09:33
 */
public interface Concost {


    /**
     * 总
     */
    String S10128E25 = "0";


    /**
     * A 相电压
     */
    String A10128E25 = "1";

    /**
     * B 相电压
     */
    String B10128E25 = "2";


    /**
     * C 相电压
     */
    String C10128E25 = "3";


    /**
     * 电压
     */
    String E215 = "10128E15";

    /**
     * 电压
     */
    String E25 = "10128E25";



    /**
     * 功率因数
     */
    String E89 = "10128E89";


    /**
     * 有功功率 s
     */

    String S10128E492 = "1";

    /**
     * 有功功率 A
     */

    String A10128E492 = "2";

    /**
     * 有功功率 B
     */

    String B10128E492 = "3";

    /**
     * 有功功率 C
     */

    String C10128E492 = "4";

    /**
     * 无功功率 S
     */

    String S10128E59 = "5";

    /**
     * 无功功率 A
     */

    String A10128E59 = "6";

    /**
     * 无功功率 B
     */

    String B10128E59 = "7";

    /**
     * 无功功率 C
     */

    String C10128E59 = "8";



    /**
     * 视在功率 S
     */

    String S10128E39 = "9";

    /**
     * 视在功率 A
     */

    String A10128E39 = "10";

    /**
     * 视在功率 B
     */

    String B10128E39 = "11";

    /**
     * 视在功率 C
     */

    String C10128E39 = "12";

    /**
     * 功率因数 S
     */

    String S10128E89 = "0";

    /**
     * 功率因数 A
     */

    String A10128E89 = "1";

    /**
     * 功率因数 B
     */

    String B10128E89 = "2";

    /**
     * 功率因数 C
     */

    String C10128E89 = "3";

    /**
     * 有功功率 10128E49
     */
    String E49 = "10128E49";

    /**
     * 有功功率 s
     */
    String S10128E49 = "1";

    /**
     * 有功功率 A
     */
    String A10128E49 = "2";


    /**
     * 有功功率 B
     */
    String B10128E49 = "3";

    /**
     * 有功功率 C
     */
    String C10128E49 = "4";

    /**
     * 10128E59  无功功率
     */
    String E59 = "10128E59";


    /**
     * 10128E39  视在功率
     */
    String E39 = "10128E39";

    /**
     * 2. 电流分钟级曲线/E_MP_I_CURVE   /10128E25
     */
    String EMPICURVE = "e_mp_i_curve";

    /**
     * 5. 日冻结电能示值/E_MP_READ_DAY
     */
    String EMP_READDAY = "e_mp_read_day";


    /**
     * 4. 功率因数分钟级曲线/E_MP_PF_CURVE
     */
    String EMPPFCURVE = "e_mp_pf_curve";


    /**
     *
     * 3. 功率分钟级曲线/E_MP_P_CURVE
     */
    String EMPPCURVE ="e_mp_p_curve";


    /**
     * 1. 电压分钟级曲线/E_MP_U_CURVE \   10128E15
     */
    String EMPUCURVE = "e_mp_u_curve";

}
